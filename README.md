# 聚合支付SDK

### MAVEN引用

```xml
<dependency>
    <groupId>cn.letspay</groupId>
    <artifactId>payment-sdk</artifactId>
    <version>1.0.0</version>
</dependency>
```

### 支付用法：
完整版：[Demo](https://gitee.com/letspay/payment-sdk/tree/master/src/test/java/example/Demo.java)
<br/>
简版：[Demo](https://gitee.com/letspay/payment-sdk/tree/master/src/test/java/example/SimpleDemo.java)
### 退款用法：[Demo](https://gitee.com/letspay/payment-sdk/tree/master/src/test/java/example/RefundDemo.java)
### 收款码收银台二维码用法：[Demo](https://gitee.com/letspay/payment-sdk/tree/master/src/test/java/example/QrCodeDemo.java)
