package example;

import cn.letspay.payment.sdk.entity.CommonResult;
import cn.letspay.payment.sdk.entity.impl.param.QrCodeSvgCreatingParam;
import cn.letspay.payment.sdk.service.PaymentService;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by liamjung on 2019-10-03.
 */
public class QrCodeDemo {

    private PaymentService paymentService;

    @Before
    public void before() {

        this.paymentService = new PaymentService();
    }

    /**
     * 创建二维码SVG
     */
    @Test
    public void createQrCodeSvg() {

        QrCodeSvgCreatingParam param = QrCodeSvgCreatingParam.builder()
                .sign("")
                .orderId("")
                .build();

        CommonResult<String> result = this.paymentService.createQrCodeSvg(param);

        System.out.println(result.toJson());
    }
}
