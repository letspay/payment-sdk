package example;

import cn.letspay.payment.sdk.entity.CommonResult;
import cn.letspay.payment.sdk.entity.attr.CashierAsyncNotificationAttr;
import cn.letspay.payment.sdk.entity.attr.CashierOrderAttr;
import cn.letspay.payment.sdk.entity.attr.CashierOrderIdAttr;
import cn.letspay.payment.sdk.entity.impl.param.BankCardManagementLinkGettingParam;
import cn.letspay.payment.sdk.entity.impl.param.CashierLinkGettingParam;
import cn.letspay.payment.sdk.entity.impl.param.CashierOrderCreatingParam;
import cn.letspay.payment.sdk.service.PaymentService;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by liamjung on 2018/10/5.
 */
public class Demo {

    private PaymentService paymentService;

    private String paymentAppId;
    private String enterprisePrivateKey;
    private String paymentPublicKey;

    private String userNo;
    private String wechatOpenId;

    @Before
    public void before() {

        this.paymentService = new PaymentService();

        this.paymentAppId = "";
        this.enterprisePrivateKey = "";
        this.paymentPublicKey = "";

        this.userNo = "";
        this.wechatOpenId = "";
    }

    /**
     * 创建收银台订单
     */
    @Test
    public void createCashierOrder() {

        CashierOrderCreatingParam param = CashierOrderCreatingParam.builder()
                .orderNo(UUID.randomUUID().toString().replace("-", ""))
                .orderName("")
                .orderAmount("")
//                .validityUnit(OrderValidityUnitEnum.天.value())
//                .validity(10)
                .returnUrl("")
                .notifyUrl("")
//                .customData()
                .build();
        param.setPaymentAppId(this.paymentAppId);
        param.setEnterprisePrivateKey(this.enterprisePrivateKey);
        param.setUserNo(this.userNo);
//        param.setIdentityNo();
//        param.setName();
//        param.setMobile();
        //开通微信支付时为必填项
        param.setWechatOpenId(this.wechatOpenId);

        CommonResult<CashierOrderAttr> result = this.paymentService.createCashierOrder(param);

        System.out.println(result.toJson());
    }

    /**
     * 获取收银台链接
     */
    @Test
    public void getCashierLink() {

        CashierLinkGettingParam param = CashierLinkGettingParam.builder()
                //二选一
//                .orderId()
//                .orderNo()
                .build();
        param.setPaymentAppId(this.paymentAppId);
        param.setEnterprisePrivateKey(this.enterprisePrivateKey);
        param.setUserNo(this.userNo);
//        param.setIdentityNo();
//        param.setName();
//        param.setMobile();
        //开通微信支付时为必填项
        param.setWechatOpenId(this.wechatOpenId);

        String link = this.paymentService.getCashierLink(param);

        System.out.println(link);
    }

    /**
     * 获取银行卡管理链接
     */
    @Test
    public void getBankCardManagementLink() {

        BankCardManagementLinkGettingParam param = BankCardManagementLinkGettingParam.builder().build();
        param.setPaymentAppId(this.paymentAppId);
        param.setEnterprisePrivateKey(this.enterprisePrivateKey);
        param.setUserNo(this.userNo);
//        param.setIdentityNo();
//        param.setName();
//        param.setMobile();
        //开通微信支付时为必填项
        param.setWechatOpenId(this.wechatOpenId);

        String link = this.paymentService.getBankCardManagementLink(param);

        System.out.println(link);
    }

    /**
     * 获取收银台订单id
     */
    @Test
    public void getCashierOrderId() {

        String requestBody = null;

        CommonResult<CashierOrderIdAttr> result = this.paymentService.getCashierOrderId(requestBody);

        System.out.println(result.toJson());
    }

    /**
     * 接收收银台异步通知
     */
    @Test
    public void receiveCashierAsyncNotification() {

        String requestBody = null;

        CommonResult<CashierAsyncNotificationAttr> result = this.paymentService.receiveCashierAsyncNotification(requestBody, this.paymentPublicKey);

        System.out.println(result.toJson());
    }
}
