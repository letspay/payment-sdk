package example;

import cn.letspay.payment.sdk.entity.CommonResult;
import cn.letspay.payment.sdk.entity.attr.CashierAsyncNotificationAttr;
import cn.letspay.payment.sdk.entity.attr.CashierOrderAttr;
import cn.letspay.payment.sdk.entity.impl.param.CashierOrderCreatingParam;
import cn.letspay.payment.sdk.service.PaymentService;
import org.junit.Before;
import org.junit.Test;

/**
 * 简版Demo
 * Created by liamjung on 2019/6/4.
 */
public class SimpleDemo {

    private PaymentService paymentService;

    //支付应用id
    private String paymentAppId;
    //企业私钥
    private String enterprisePrivateKey;
    //支付公钥
    private String paymentPublicKey;

    //接入平台用户编号（需保证唯一）
    private String userNo;
    //微信openId
    private String wechatOpenId;

    @Before
    public void before() {

        this.paymentService = new PaymentService();

        this.paymentAppId = "";
        this.enterprisePrivateKey = "";
        this.paymentPublicKey = "";

        this.userNo = "";
        this.wechatOpenId = "";
    }

    /**
     * 创建收银台订单
     */
    @Test
    public void createCashierOrder() {

        CashierOrderCreatingParam param = CashierOrderCreatingParam.builder()
                //接入平台订单编号（需保证唯一）
                .orderNo("")
                //订单名称
                .orderName("")
                //订单金额
                .orderAmount("")

                //同步返回url，不需要时，可不填
//                .returnUrl()
                //异步通知url
                .notifyUrl("")
                //自定义数据，用于异步通知接入平台，不需要时，可不填
//                .customData()
                .build();

        param.setPaymentAppId(this.paymentAppId);
        param.setEnterprisePrivateKey(this.enterprisePrivateKey);

        param.setUserNo(this.userNo);
        param.setWechatOpenId(this.wechatOpenId);

        CommonResult<CashierOrderAttr> result = this.paymentService.createCashierOrder(param);

        System.out.println(result.toJson());
    }

    /**
     * 接收收银台异步通知
     */
    @Test
    public void receiveCashierAsyncNotification() {

        String requestBody = null;

        CommonResult<CashierAsyncNotificationAttr> result = this.paymentService.receiveCashierAsyncNotification(requestBody, this.paymentPublicKey);

        System.out.println(result.toJson());
    }
}
