package example;

import cn.letspay.payment.sdk.entity.CommonResult;
import cn.letspay.payment.sdk.entity.attr.RefundAsyncNotificationAttr;
import cn.letspay.payment.sdk.entity.attr.RefundAttr;
import cn.letspay.payment.sdk.entity.impl.param.RefundApplyingParam;
import cn.letspay.payment.sdk.service.PaymentService;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by liamjung on 2019-06-12.
 */
public class RefundDemo {

    private PaymentService paymentService;

    private String paymentAppId;
    private String enterprisePrivateKey;
    private String paymentPublicKey;

    @Before
    public void before() {

        this.paymentService = new PaymentService();

        this.paymentAppId = "";
        this.enterprisePrivateKey = "";
        this.paymentPublicKey = "";
    }

    /**
     * 申请退款
     */
    @Test
    public void applyRefund() {

        RefundApplyingParam param = RefundApplyingParam.builder()
                .refundNo("")
                .orderId("")
                .refundAmount("")
//                .description()
                .notifyUrl("")
//                .customData()
                .build();
        param.setPaymentAppId(this.paymentAppId);
        param.setEnterprisePrivateKey(this.enterprisePrivateKey);

        CommonResult<RefundAttr> result = this.paymentService.applyRefund(param);

        System.out.println(result.toJson());
    }

    /**
     * 接收退款异步通知
     */
    @Test
    public void receiveRefundAsyncNotification() {

        String requestBody = null;

        CommonResult<RefundAsyncNotificationAttr> result = this.paymentService.receiveRefundAsyncNotification(requestBody, this.paymentPublicKey);

        System.out.println(result.toJson());
    }
}
