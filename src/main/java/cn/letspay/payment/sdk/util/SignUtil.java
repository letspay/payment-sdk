package cn.letspay.payment.sdk.util;

import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 * 签名工具类
 * Created by liamjung on 2018/9/26.
 */
public class SignUtil {


    public static String fomatParameter(Map<String, Object> map, String separator) {
        List<String> list = new ArrayList<>(map.keySet());
        list.sort(String::compareToIgnoreCase);
        StringBuilder sb = new StringBuilder();
        list.forEach(s -> {
            Object v = map.get(s);
            if (v != null && !"".equals(v)) {
                sb.append(s).append("=").append(map.get(s)).append(separator);
            }
        });
        return sb.toString().substring(0, sb.length() - 1);
    }


    public static String createSign(String data, String privateKey) {
        try {
            return Base64.getEncoder()
                    .encodeToString(sign(data.getBytes("UTF-8"), getPrivateKey(privateKey)))
                    .replaceAll("\r", "")
                    .replaceAll("\n", "")
                    .replaceAll("\t", "");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean verySignSimple(String signData, String pubKey, String sign) {
        try {
            return verify(signData.getBytes("UTF-8"), getPublicKey(pubKey), Base64.getDecoder().decode(sign));
        } catch (Exception e) {
            return false;
        }
    }

    public static PublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes = Base64.getDecoder().decode(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(keySpec);
    }

    public static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes = Base64.getDecoder().decode(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(keySpec);
    }

    public static byte[] sign(byte[] cipherText, PrivateKey privateKey) throws Exception {
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initSign(privateKey);
        signature.update(cipherText);
        return signature.sign();
    }

    public static boolean verify(byte[] cipherText, PublicKey publicKey, byte[] sign) throws Exception {
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initVerify(publicKey);
        signature.update(cipherText);
        return signature.verify(sign);
    }

    public static String Base64Encoder(String data) {
        try {
            return Base64.getEncoder().encodeToString(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String Base64Decoder(String data) {
        try {
            return new String(Base64.getDecoder().decode(data), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
