package cn.letspay.payment.sdk.entity.impl.param;

import cn.letspay.payment.sdk.entity.BaseParam;
import cn.letspay.payment.sdk.enumeration.OrderValidityUnitEnum;
import lombok.*;

/**
 * 创建收银台订单参数
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CashierOrderCreatingParam extends BaseParam {

    /**
     * 接入平台订单编号（需保证唯一）
     */
    private String orderNo;

    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 订单金额
     */
    private String orderAmount;

    /**
     * 订单有效期单位（可选）
     * 默认值：分钟
     * 参考{@link OrderValidityUnitEnum}枚举value
     */
    private Integer validityUnit;

    /**
     * 订单有效期（可选）
     * 默认值：1天
     */
    private Integer validity;

    /**
     * 同步返回url（可选）
     */
    private String returnUrl;

    /**
     * 异步通知url
     */
    private String notifyUrl;

    /**
     * 自定义数据，用于异步通知（可选）
     */
    private String customData;
}
