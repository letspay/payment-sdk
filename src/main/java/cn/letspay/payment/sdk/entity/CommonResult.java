package cn.letspay.payment.sdk.entity;

import cn.letspay.payment.sdk.enumeration.ErrorCodeEnum;
import com.alibaba.fastjson.JSON;
import lombok.*;

/**
 * 通用结果实体
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommonResult<T> extends BaseResult {

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 响应数据
     */
    private T data;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 构造成功结果实体
     *
     * @return 通用结果
     */
    public static CommonResult success() {
        return success(null);
    }

    /**
     * 构造成功结果实体
     *
     * @param data 数据
     * @param <T>  数据类泛型
     * @return 通用结果
     */
    public static <T> CommonResult<T> success(T data) {
        return (CommonResult<T>) CommonResult.builder()
                .success(true)
                .data(data)
                .timestamp(System.currentTimeMillis())
                .build();
    }

    /**
     * 构造失败结果实体
     *
     * @param errorCodeEnum 错误码枚举
     * @param <T>           数据类泛型
     * @return 通用结果
     */
    public static <T> CommonResult<T> error(ErrorCodeEnum errorCodeEnum) {
        return error(errorCodeEnum.value(), errorCodeEnum.msg());
    }

    /**
     * 构造失败结果实体
     *
     * @param errorCode 错误码
     * @param errorMsg  错误信息
     * @return 通用结果
     */
    public static CommonResult error(String errorCode, String errorMsg) {
        return CommonResult.builder()
                .success(false)
                .errorCode(errorCode)
                .errorMsg(errorMsg)
                .timestamp(System.currentTimeMillis())
                .build();
    }

    /**
     * 解析
     *
     * @param json      json字符串
     * @param entityCls 实体类
     * @param <E>       实体类泛型
     * @return 通用结果
     */
    public static <E> CommonResult<E> parse(String json, Class<E> entityCls) {

        CommonResult tempCommonResult = JSON.parseObject(json, CommonResult.class);

        CommonResult<E> commonResult = new CommonResult<>();
        commonResult.setSuccess(tempCommonResult.getSuccess());
        commonResult.setErrorCode(tempCommonResult.getErrorCode());
        commonResult.setErrorMsg(tempCommonResult.getErrorMsg());
        commonResult.setTimestamp(tempCommonResult.getTimestamp());

        if (entityCls == String.class)
            commonResult.setData((E) tempCommonResult.getData());
        else if (entityCls != Void.class)
            commonResult.setData(JSON.toJavaObject((JSON) tempCommonResult.getData(), entityCls));

        return commonResult;
    }
}
