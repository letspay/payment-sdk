package cn.letspay.payment.sdk.entity.attr;

import lombok.*;

/**
 * 个人用户签名属性
 * Created by liamjung on 2018/10/8.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IndividualSignAttr {

    /**
     * 应用id
     */
    private String appId;

    /**
     * 接入平台用户编号（需保证唯一）
     */
    private String userNo;

    /**
     * 身份证号（可选）
     * 仅用于快捷绑卡
     */
    private String identityNo;

    /**
     * 姓名（可选）
     * 仅用于快捷绑卡
     */
    private String name;

    /**
     * 手机号（可选）
     * 仅用于快捷绑卡
     */
    private String mobile;

    /**
     * 微信openId（可选）
     * 开通微信支付时为必填项
     */
    private String wechatOpenId;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 签名
     */
    private byte[] sign;
}
