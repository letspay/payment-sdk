package cn.letspay.payment.sdk.entity.impl.param;

import cn.letspay.payment.sdk.entity.BaseEntity;
import lombok.*;

/**
 * 创建二维码SVG参数
 * Created by liamjung on 2019-10-03.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QrCodeSvgCreatingParam extends BaseEntity {

    /**
     * 签名
     */
    private String sign;

    /**
     * 订单id
     */
    private String orderId;
}
