package cn.letspay.payment.sdk.entity.attr;

import cn.letspay.payment.sdk.entity.BaseResult;
import cn.letspay.payment.sdk.enumeration.RefundStatusEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 收银台异步通知属性
 * Created by liamjung on 2019-06-11.
 */
@Getter
@Setter
public class RefundAsyncNotificationAttr extends BaseResult {

    /**
     * 退款id
     */
    private String refundId;

    /**
     * 接入平台退款编号
     */
    private String refundNo;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 接入平台订单编号
     */
    private String orderNo;

    /**
     * 退款金额
     */
    private String amount;

    /**
     * 退款手续费
     * 要退的手续费，按比例计算
     */
    private String fee;

    /**
     * 退款总金额（= 退款金额 + 退款手续费）
     */
    private String totalAmount;

    /**
     * 退款状态
     * 参考{@link RefundStatusEnum}枚举
     */
    private Integer refundStatus;

    /**
     * 自定义数据
     */
    private String customData;

    /**
     * 时间戳
     */
    private Long timestamp;
}
