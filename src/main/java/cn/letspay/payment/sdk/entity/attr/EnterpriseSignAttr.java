package cn.letspay.payment.sdk.entity.attr;

import lombok.*;

/**
 * 企业用户签名属性
 * Created by liamjung on 2019-06-11.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EnterpriseSignAttr {

    /**
     * 应用id
     */
    private String appId;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 签名
     */
    private byte[] sign;
}
