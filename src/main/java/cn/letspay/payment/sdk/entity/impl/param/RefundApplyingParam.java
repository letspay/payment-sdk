package cn.letspay.payment.sdk.entity.impl.param;

import cn.letspay.payment.sdk.entity.BaseParam;
import lombok.*;

/**
 * 申请退款参数
 * Created by liamjung on 2019-06-11.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RefundApplyingParam extends BaseParam {

    /**
     * 接入平台退款编号（需保证唯一）
     */
    private String refundNo;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 退款金额
     */
    private String refundAmount;

    /**
     * 退款说明（可选）
     */
    private String description;

    /**
     * 异步通知url
     */
    private String notifyUrl;

    /**
     * 自定义数据，用于异步通知（可选）
     */
    private String customData;
}
