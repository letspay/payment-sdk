package cn.letspay.payment.sdk.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

/**
 * 抽象参数
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
public abstract class BaseParam extends BaseEntity {

    /**
     * 聚合支付应用id
     */
    @JSONField(serialize = false)
    private String paymentAppId;

    /**
     * 企业用户私钥（企业保留，用于加密调用支付系统时的数据）
     */
    @JSONField(serialize = false)
    private String enterprisePrivateKey;

    /**
     * 接入平台用户编号（需保证唯一）
     */
    @JSONField(serialize = false)
    private String userNo;

    /**
     * 身份证号（可选）
     * 仅用于快捷绑卡
     */
    @JSONField(serialize = false)
    private String identityNo;

    /**
     * 姓名（可选）
     * 仅用于快捷绑卡
     */
    @JSONField(serialize = false)
    private String name;

    /**
     * 手机号（可选）
     * 仅用于快捷绑卡
     */
    @JSONField(serialize = false)
    private String mobile;

    /**
     * 微信openId（可选）
     * 开通微信JSAPI相关的支付时为必填项
     */
    @JSONField(serialize = false)
    private String wechatOpenId;
}
