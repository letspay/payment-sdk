package cn.letspay.payment.sdk.entity.attr;

import lombok.*;

/**
 * 收银台订单id属性
 * Created by liamjung on 2018/10/18.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CashierOrderIdAttr {

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 接入平台订单编号
     */
    private String orderNo;

    /**
     * 随机id
     * 支付网关业务单号
     */
    private String randomId;

    /**
     * 自定义数据，用于异步通知
     */
    private String customData;
}
