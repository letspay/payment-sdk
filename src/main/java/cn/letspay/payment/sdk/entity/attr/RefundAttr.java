package cn.letspay.payment.sdk.entity.attr;

import lombok.Getter;
import lombok.Setter;

/**
 * 退款属性
 * Created by liamjung on 2019-06-11.
 */
@Getter
@Setter
public class RefundAttr {

    /**
     * 退款id
     */
    private String refundId;

    /**
     * 接入平台退款编号
     */
    private String refundNo;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 接入平台订单编号
     */
    private String orderNo;
}
