package cn.letspay.payment.sdk.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 抽象结果
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
public abstract class BaseResult extends BaseEntity {

}
