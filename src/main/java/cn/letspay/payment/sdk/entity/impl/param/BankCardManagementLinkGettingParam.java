package cn.letspay.payment.sdk.entity.impl.param;

import cn.letspay.payment.sdk.entity.BaseParam;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 获取银行卡管理链接参数
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Builder
public class BankCardManagementLinkGettingParam extends BaseParam {

}
