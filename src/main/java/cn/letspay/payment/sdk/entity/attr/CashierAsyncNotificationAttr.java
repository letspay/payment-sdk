package cn.letspay.payment.sdk.entity.attr;

import cn.letspay.payment.sdk.entity.BaseResult;
import cn.letspay.payment.sdk.enumeration.CashierPaymentProductIdEnum;
import cn.letspay.payment.sdk.enumeration.OrderStatusEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 收银台异步通知属性
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
public class CashierAsyncNotificationAttr extends BaseResult {

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 接入平台订单编号
     */
    private String orderNo;

    /**
     * 随机id
     * 支付网关业务单号
     */
    private String randomId;

    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 订单金额
     */
    private String orderAmount;

    /**
     * 支付产品id
     * 参考{@link CashierPaymentProductIdEnum}枚举
     */
    private String paymentProductId;

    /**
     * 支付产品名称
     * 参考{@link CashierPaymentProductIdEnum}枚举
     */
    private String paymentProductName;

    /**
     * 支付手续费
     */
    private String paymentFee;

    /**
     * 支付金额（= 订单金额 + 支付手续费）
     */
    private String paymentAmount;

    /**
     * 企业手续费
     */
    private String enterpriseFee;

    /**
     * 订单状态
     * 参考{@link OrderStatusEnum}枚举
     */
    private Integer orderStatus;

    /**
     * 自定义数据
     */
    private String customData;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 返回支付产品id枚举
     *
     * @return 支付产品id枚举
     */
    public CashierPaymentProductIdEnum paymentProductId() {
        return CashierPaymentProductIdEnum.parse(this.paymentProductId);
    }
}
