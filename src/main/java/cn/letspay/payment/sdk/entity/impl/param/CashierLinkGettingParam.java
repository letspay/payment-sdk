package cn.letspay.payment.sdk.entity.impl.param;

import cn.letspay.payment.sdk.entity.BaseParam;
import lombok.*;

/**
 * 获取收银台链接参数
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CashierLinkGettingParam extends BaseParam {

    /**
     * 订单id（可选）
     * 与orderNo二选一
     */
    private String orderId;

    /**
     * 接入平台订单编号（可选）
     * 与orderId二选一
     */
    private String orderNo;
}
