package cn.letspay.payment.sdk.entity.attr;

import lombok.Getter;
import lombok.Setter;

/**
 * 收银台订单属性
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
public class CashierOrderAttr {

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 接入平台订单编号
     */
    private String orderNo;

    /**
     * 签名
     * 用于后续流程
     */
    private String sign;

    /**
     * 收银台链接
     */
    private String cashierLink;
}
