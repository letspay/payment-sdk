package cn.letspay.payment.sdk.entity;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

/**
 * 抽象实体
 * Created by liamjung on 2018/10/5.
 */
@Getter
@Setter
public abstract class BaseEntity {

    /**
     * 转json字符串
     *
     * @return json字符串
     */
    public final String toJson() {
        return JSON.toJSONString(this);
    }
}
