package cn.letspay.payment.sdk.enumeration;

/**
 * 退款状态枚举
 * Created by liamjung on 2019-06-11.
 */
public enum RefundStatusEnum {

    退款成功(2),
    退款失败(3);

    private Integer value;

    RefundStatusEnum(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }
}
