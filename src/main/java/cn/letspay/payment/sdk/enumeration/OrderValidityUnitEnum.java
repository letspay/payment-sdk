package cn.letspay.payment.sdk.enumeration;

/**
 * 订单有效期单位枚举
 * Created by liamjung on 2018/9/27.
 */
public enum OrderValidityUnitEnum {

    天(0),
    小时(1),
    分钟(2);

    private Integer value;

    OrderValidityUnitEnum(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }
}
