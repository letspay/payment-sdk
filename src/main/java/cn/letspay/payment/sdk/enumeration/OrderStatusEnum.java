package cn.letspay.payment.sdk.enumeration;

/**
 * 收银台订单状态
 * Created by liamjung on 2018/9/25.
 */
public enum OrderStatusEnum {

    支付成功(3),
    支付失败(4);

    private Integer value;

    OrderStatusEnum(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }
}
