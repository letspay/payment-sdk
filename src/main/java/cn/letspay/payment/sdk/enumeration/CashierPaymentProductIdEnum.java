package cn.letspay.payment.sdk.enumeration;

import cn.letspay.payment.sdk.util.EnumUtil;

/**
 * 收银台支付产品id枚举
 * Created by liamjung on 2018/9/27.
 */
public enum CashierPaymentProductIdEnum {

    /**
     * 信用卡分期支付
     */
    信用卡分期支付("credit_card_instalment_pay"),

    /**
     * 储蓄卡支付
     */
    银联支付("union_pay"),

    /**
     * 第三方WEB支付
     */
    微信JSAPI支付("wechat_jsapi_pay"),
    微信H5支付("wechat_h5_pay"),
    支付宝手机网站支付("alipay_wap_pay"),

    /**
     * 第三方APP支付
     */
    微信APP支付("wechat_app_pay"), 支付宝APP支付("alipay_app_pay"),

    /**
     * 第三方小程序支付
     */
    微信小程序支付("wechat_mini_app_pay");

    private String value;

    CashierPaymentProductIdEnum(String value) {
        this.value = value;
    }

    /**
     * 返回枚举取值
     *
     * @return 枚举取值
     */
    public String value() {
        return value;
    }

    /**
     * 解析
     *
     * @param value 枚举取值
     * @return 枚举对象
     */
    public static CashierPaymentProductIdEnum parse(String value) {
        return EnumUtil.parse(value, CashierPaymentProductIdEnum.class);
    }
}
