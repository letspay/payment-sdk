package cn.letspay.payment.sdk.enumeration;

/**
 * 错误码枚举
 * Created by liamjung on 2018/10/5.
 */
public enum ErrorCodeEnum {

    SIGN_INSPECTION_FAILED("SIGN_INSPECTION_FAILED", "验签失败"),
    SIGN_EXPIRED("SIGN_EXPIRED", "签名已过期");

    private String value;
    private String msg;

    ErrorCodeEnum(String value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public String value() {
        return value;
    }

    public String msg() {
        return msg;
    }
}
