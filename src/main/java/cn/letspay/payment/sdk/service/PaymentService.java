package cn.letspay.payment.sdk.service;

import cn.letspay.payment.sdk.entity.BaseParam;
import cn.letspay.payment.sdk.entity.CommonResult;
import cn.letspay.payment.sdk.entity.attr.*;
import cn.letspay.payment.sdk.entity.impl.param.*;
import cn.letspay.payment.sdk.enumeration.ErrorCodeEnum;
import cn.letspay.payment.sdk.exception.SignExpiredException;
import cn.letspay.payment.sdk.exception.SignFailedException;
import cn.letspay.payment.sdk.exception.SignInspectionFailedException;
import cn.letspay.payment.sdk.util.SignUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;

/**
 * 聚合支付服务
 * Created by liamjung on 2018/10/5.
 */
@Slf4j
@Service
public class PaymentService {

    @Value("${letspay.host}")
    private String paymentHost = "https://cashier.letspay.cn";

    private final RestTemplate REST_TEMPLATE;

    /**
     * 默认构造方法
     * 用于spring上下文
     */
    public PaymentService() {

        //解决Invalid HTTP method: PATCH问题
        REST_TEMPLATE = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        //解决中文乱码
        //RestTemplate默认构造方法中StringHttpMessageConverter在list中的索引是1
        REST_TEMPLATE.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
    }

    /**
     * 有参构造方法
     * 用于非spring上下文
     *
     * @param paymentHost 聚合支付主机（http(s)://xxx:xxx(:xxx)）
     */
    public PaymentService(String paymentHost) {
        this();

        this.paymentHost = paymentHost;
    }

    /**
     * 创建收银台订单
     *
     * @param param 创建收银台订单参数
     * @return 收银台订单属性结果
     */
    public CommonResult<CashierOrderAttr> createCashierOrder(CashierOrderCreatingParam param) {

        String uri = "/individual/v1/cashiers/order";

        JSONObject paramJson = this.sign(param);

        CommonResult<CashierOrderAttr> result = this.execute(uri, HttpMethod.POST, paramJson, CashierOrderAttr.class);

        if (result.getSuccess()) {
            result.getData().setSign(paramJson.getString("sign"));

            CashierLinkGettingParam p = CashierLinkGettingParam.builder()
                    .orderId(result.getData().getOrderId())
                    .build();
            p.setPaymentAppId(param.getPaymentAppId());
            p.setEnterprisePrivateKey(param.getEnterprisePrivateKey());
            p.setUserNo(param.getUserNo());
            p.setName(param.getName());
            p.setMobile(param.getMobile());
            p.setIdentityNo(param.getIdentityNo());
            p.setWechatOpenId(param.getWechatOpenId());

            String link = this.getCashierLink(p);

            result.getData().setCashierLink(link);
        }

        return result;
    }

    /**
     * 获取收银台链接
     *
     * @param param 获取收银台链接参数
     * @return 收银台url
     */
    public String getCashierLink(CashierLinkGettingParam param) {

        String uri = "/cashier_payRent";

        return this.generateLink(uri, param);
    }

    /**
     * 获取银行卡管理链接
     *
     * @param param 获取银行卡管理链接参数
     * @return 银行卡管理url
     */
    public String getBankCardManagementLink(BankCardManagementLinkGettingParam param) {

        String uri = "/cashier_bankList";

        return this.generateLink(uri, param);
    }

    /**
     * 获取收银台订单id
     *
     * @param requestBody 异步通知请求体
     * @return 收银台订单id属性结果
     */
    public CommonResult<CashierOrderIdAttr> getCashierOrderId(String requestBody) {

        CashierAsyncNotificationAttr attr = JSON.parseObject(requestBody, CashierAsyncNotificationAttr.class);

        return CommonResult.success(CashierOrderIdAttr.builder()
                .orderId(attr.getOrderId())
                .orderNo(attr.getOrderNo())
                .randomId(attr.getRandomId())
                .customData(attr.getCustomData())
                .build());
    }

    /**
     * 接收收银台异步通知
     *
     * @param requestBody      异步通知请求体
     * @param paymentPublicKey 公钥（需提供给接入平台，用于验签）
     * @return 收银台异步通知属性结果
     */
    public CommonResult<CashierAsyncNotificationAttr> receiveCashierAsyncNotification(String requestBody, String paymentPublicKey) {
        return this.receiveAsyncNotification(requestBody, paymentPublicKey, CashierAsyncNotificationAttr.class);
    }

    //申请退款
    public CommonResult<RefundAttr> applyRefund(RefundApplyingParam param) {

        String uri = "/enterprise/v1/refunds";

        JSONObject paramJson = this.signEnt(param);

        CommonResult<RefundAttr> result = this.execute(uri, HttpMethod.POST, paramJson, RefundAttr.class);

        return result;
    }

    /**
     * 接收退款异步通知
     *
     * @param requestBody      异步通知请求体
     * @param paymentPublicKey 公钥（需提供给接入平台，用于验签）
     * @return 退款异步通知属性结果
     */
    public CommonResult<RefundAsyncNotificationAttr> receiveRefundAsyncNotification(String requestBody, String paymentPublicKey) {
        return this.receiveAsyncNotification(requestBody, paymentPublicKey, RefundAsyncNotificationAttr.class);
    }

    /**
     * 创建二维码SVG
     *
     * @param param 创建二维码SVG参数
     * @return 二维码SVG
     */
    public CommonResult<String> createQrCodeSvg(QrCodeSvgCreatingParam param) {

        String uri = "/individual/v1/cashiers/qr_code/svg";

        CommonResult<String> result = this.execute(uri, HttpMethod.POST, param, String.class);

        return result;
    }


    /**
     * 接收异步通知
     *
     * @param requestBody      异步通知请求体
     * @param paymentPublicKey 公钥（需提供给接入平台，用于验签）
     * @param attrCls
     * @param <A>
     * @return 异步通知属性结果
     */
    private <A> CommonResult<A> receiveAsyncNotification(String requestBody, String paymentPublicKey, Class<A> attrCls) {

        CommonResult<A> result;

        try {
            //验签
            JSONObject jsonObject = this.inspectSign(requestBody, paymentPublicKey);

            A attr = JSON.toJavaObject(jsonObject, attrCls);

            result = CommonResult.success(attr);

        } catch (SignExpiredException see) {
            result = CommonResult.error(ErrorCodeEnum.SIGN_EXPIRED);
        } catch (SignInspectionFailedException sife) {
            result = CommonResult.error(ErrorCodeEnum.SIGN_INSPECTION_FAILED);
        }

        return result;
    }

    /**
     * 执行
     *
     * @param uri       请求uri
     * @param method    请求方法
     * @param paramJson 参数
     * @param attrCls   属性类
     * @param <A>       属性类泛型
     * @return 通用结果
     */
    private <A> CommonResult<A> execute(String uri, HttpMethod method, Object paramJson, Class<A> attrCls) {

        String url = this.paymentHost + uri;

        HttpHeaders headers = new HttpHeaders();
        //解决问题：ResourceAccessException: I/O error...
        headers.set("Connection", "Close");
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.add("Accept", "application/json");

        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(paramJson), headers);

        String json;

        try {
            ResponseEntity<String> resp = REST_TEMPLATE.exchange(url, method, httpEntity, String.class);

            json = resp.getBody();
        } catch (HttpClientErrorException hcee) {

            log.warn(hcee.getMessage());

            json = hcee.getResponseBodyAsString();
        }

        return CommonResult.parse(json, attrCls);
    }

    /**
     * 生成链接
     *
     * @param uri   请求uri
     * @param param 参数
     * @return url
     */
    private String generateLink(String uri, BaseParam param) {

        JSONObject jsonObject = this.sign(param);

        String queryString = this.jsonObjectToQueryString(jsonObject);

        String url = this.paymentHost + uri + "?" + queryString;

        return url;
    }

    /**
     * 个人签名
     * 签名方式：用RSA私钥，对《应用id_身份证号_手机号_姓名_接入平台用户编号_微信openId_时间戳》进行MD5签名，然后转base64
     *
     * @param param 参数
     * @return json对象
     */
    private JSONObject sign(BaseParam param) {

        JSONObject jsonObject = JSON.parseObject(param.toJson());

        IndividualSignAttr individualSign = IndividualSignAttr.builder()
                .userNo(param.getUserNo())
                .identityNo(param.getIdentityNo())
                .name(param.getName())
                .mobile(param.getMobile())
                .wechatOpenId(param.getWechatOpenId())
                //待扩展
                .build();
        individualSign.setAppId(param.getPaymentAppId());
        individualSign.setTimestamp(System.currentTimeMillis());

        try {
            String cipherStr = param.getPaymentAppId() + "_" +
                    param.getIdentityNo() + "_" +
                    param.getMobile() + "_" +
                    param.getName() + "_" +
                    param.getUserNo() + "_" +
                    param.getWechatOpenId() + "_" +
                    individualSign.getTimestamp();

            byte[] cipherText = cipherStr.getBytes(StandardCharsets.UTF_8);
            PrivateKey privateKey = SignUtil.getPrivateKey(param.getEnterprisePrivateKey());

            byte[] bytes = SignUtil.sign(cipherText, privateKey);

            individualSign.setSign(bytes);

            String signPlaintext = JSON.toJSONString(individualSign);

            String sign = Base64.getEncoder().encodeToString(signPlaintext.getBytes());

            jsonObject.put("sign", sign);

        } catch (Exception e) {
            log.error(e.getMessage(), e);

            throw new SignFailedException(e);
        }

        return jsonObject;
    }

    /**
     * 企业签名
     * 签名方式：用RSA私钥，对《应用id_时间戳》进行MD5签名，然后转base64
     *
     * @param param
     * @return
     */
    private JSONObject signEnt(RefundApplyingParam param) {

        JSONObject jsonObject = JSON.parseObject(param.toJson());

        EnterpriseSignAttr enterpriseSignAttr = EnterpriseSignAttr.builder()
                .appId(param.getPaymentAppId())
                .timestamp(System.currentTimeMillis())
                .build();

        try {
            String cipherStr = param.getPaymentAppId() + "_" +
                    enterpriseSignAttr.getTimestamp();

            byte[] cipherText = cipherStr.getBytes(StandardCharsets.UTF_8);
            PrivateKey privateKey = SignUtil.getPrivateKey(param.getEnterprisePrivateKey());

            byte[] bytes = SignUtil.sign(cipherText, privateKey);

            enterpriseSignAttr.setSign(bytes);

            String signPlaintext = JSON.toJSONString(enterpriseSignAttr);

            String sign = Base64.getEncoder().encodeToString(signPlaintext.getBytes());

            jsonObject.put("sign", sign);

        } catch (Exception e) {
            log.error(e.getMessage(), e);

            throw new SignFailedException(e);
        }

        return jsonObject;
    }

    /**
     * 验签
     * 验签方式：根据《字段名=字段值》生序排序后，用RSA公钥，对《字段名1=字段值1&字段名2=字段值2&...》（除sign字段外）进行MD5验签
     *
     * @param data             异步通知请求体
     * @param paymentPublicKey 公钥（需提供给接入平台，用于验签）
     * @return json对象
     */
    private JSONObject inspectSign(String data, String paymentPublicKey) {

        JSONObject jsonObject;

        try {
//            String json = new String(Base64.getDecoder().decode(data));
//
//            jsonObject = JSON.parseObject(json);
            jsonObject = JSON.parseObject(data);
            byte[] sign = jsonObject.getBytes("sign");
            //验签时，先移除签名
            jsonObject.remove("sign");

            List<String> list = new ArrayList<>();

            Set<Map.Entry<String, Object>> entries = jsonObject.entrySet();

            for (Map.Entry<String, Object> entry : entries)
                list.add(entry.getKey() + "=" + entry.getValue());

            //生序排序
            list.sort(String::compareTo);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                String str = list.get(i);

                sb.append(str);

                if (i < list.size() - 1)
                    sb.append("&");
            }

            byte[] cipherText = sb.toString().getBytes(StandardCharsets.UTF_8);
            PublicKey publicKey = SignUtil.getPublicKey(paymentPublicKey);

            boolean flag = SignUtil.verify(cipherText, publicKey, sign);

            if (!flag) {
                //抛出验签失败异常
                throw new SignInspectionFailedException();
            }

            Long timestamp = jsonObject.getLong("signTimestamp");
            Long signValidity = jsonObject.getLong("signValidity");

            long expiryTimestamp = timestamp + signValidity;

            if (expiryTimestamp <= System.currentTimeMillis()) {
                //抛出签名已过期异常
                throw new SignExpiredException();
            }
        } catch (SignInspectionFailedException | SignExpiredException se) {

            throw se;
        } catch (Exception e) {
            log.error(e.getMessage(), e);

            throw new SignInspectionFailedException();
        }

        return jsonObject;
    }

    /**
     * json对象转qs
     *
     * @param jsonObject json对象
     * @return query string
     */
    private String jsonObjectToQueryString(JSONObject jsonObject) {

        StringBuilder sb = new StringBuilder();

        for (Map.Entry entry : jsonObject.entrySet()) {
            try {
                sb.append(entry.getKey())
                        .append("=")
                        .append(URLEncoder.encode(entry.getValue() + "", "UTF-8"))
                        .append("&");
            } catch (UnsupportedEncodingException uee) {
                log.error(uee.getMessage(), uee);

                throw new RuntimeException(uee);
            }
        }

        String qs = sb.toString();
        qs = qs.substring(0, qs.length() - 1);

        return qs;
    }
}
