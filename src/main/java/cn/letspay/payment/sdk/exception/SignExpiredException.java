package cn.letspay.payment.sdk.exception;

/**
 * 签名已过期
 * Created by liamjung on 2018/10/5.
 */
public class SignExpiredException extends RuntimeException {
}
