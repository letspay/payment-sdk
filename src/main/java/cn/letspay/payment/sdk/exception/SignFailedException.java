package cn.letspay.payment.sdk.exception;

/**
 * 签名失败异常
 * Created by liamjung on 2018/10/5.
 */
public class SignFailedException extends RuntimeException {

    public SignFailedException(Throwable cause) {
        super(cause);
    }
}
